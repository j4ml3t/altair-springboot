package com.altair.exercise.domain;

public interface Console {

    void printLine(String text);
}
