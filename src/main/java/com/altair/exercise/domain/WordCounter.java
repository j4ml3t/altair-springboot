package com.altair.exercise.domain;

import com.altair.exercise.infrastructure.storage.TextFileReader;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

import static java.lang.String.format;

@Service
@AllArgsConstructor
public class WordCounter {

	private final TextFileReader textFileReader;
	private final WordPrinter wordPrinter;
	private final TextFileName textFileName;

	@PostConstruct
	public void processWordsFromFile() throws IOException {
		final List<String> lines = textFileReader.readLinesFromFile(textFileName.getName());

		final HashMap<String, Integer> wordMap = getWordMapFromLines(lines);

		final List<Map.Entry<String, Integer>> wordOrderedList = getOrderedListOfWordsDesc(wordMap);

		wordPrinter.printWordsOrderByAppearanceDesc(wordOrderedList);
	}

	private HashMap<String, Integer> getWordMapFromLines(final List<String> lines) {
		final HashMap<String, Integer> wordMap = new HashMap<>();
		
		for (String line: lines) {
			for (String word: getWordsFromLine(line)) {

				if (wordMap.containsKey(word)) {
					wordMap.put(word, wordMap.get(word)+1);
				} else {
					wordMap.put(word, 1);
				}
			}
		}
		return wordMap;
	}

	private ArrayList<Map.Entry<String, Integer>> getOrderedListOfWordsDesc(final HashMap<String,Integer> wordMap) {
		return new ArrayList<>(wordMap.entrySet());
	}

	private String[] getWordsFromLine(String line) {
		return line.replaceAll("[^a-zA-Z ]", "")
					.toLowerCase()
					.split("\\s+");
	}

}
