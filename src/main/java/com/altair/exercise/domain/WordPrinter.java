package com.altair.exercise.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Service
@AllArgsConstructor
public class WordPrinter {

    public static final String WORD_OUTPUT = "-  Word: %s  >> Occurrences: %s";

    private final Console console;

    public void printWordsOrderByAppearanceDesc(final List<Map.Entry<String, Integer>> wordOrderedList) {

        Collections.sort(wordOrderedList, (word1, word2) -> word2.getValue().compareTo(word1.getValue()));

        wordOrderedList.forEach(entry ->
                console.printLine(format(WORD_OUTPUT, entry.getKey(), entry.getValue())));
    }
}
