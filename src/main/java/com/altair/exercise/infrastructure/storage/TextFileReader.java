package com.altair.exercise.infrastructure.storage;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
@AllArgsConstructor
public class TextFileReader {

    private final FileStorage filePathStorage;

    public List<String> readLinesFromFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(filePathStorage.getFile(fileName).getAbsolutePath()));
    }

}
