package com.altair.exercise.infrastructure.storage;

import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

import static java.lang.String.format;

@Component
public class FileStorage {

    private static final String CLASSPATH = "classpath:%s";

    public File getFile(String fileName) throws FileNotFoundException {
        return ResourceUtils.getFile(format(CLASSPATH, fileName));
    }
}
