package com.altair.exercise.infrastructure;

import com.altair.exercise.domain.Console;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ConsoleSlf4 implements Console {

    @Override
    public void printLine(String text) {
        log.info(text);
    }
}
