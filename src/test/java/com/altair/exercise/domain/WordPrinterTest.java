package com.altair.exercise.domain;

import com.altair.exercise.infrastructure.ConsoleSlf4;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WordPrinterTest {

    private WordPrinter wordPrinter;
    @Mock
    private ConsoleSlf4 console;

    @BeforeEach
    public void initialize() {
        wordPrinter = new WordPrinter(console);
    }

    @Test
    public void Should_CallConsoleInRightOrder_WhenPrintingWordsDesc() {

        // Given
        String firstStr = "First";
        final Integer firstInt = 1;
        String secondStr = "Second";
        final Integer secondInt = 2;

        List<Map.Entry<String, Integer>> wordEntryList = new ArrayList<>();
        wordEntryList.add(new AbstractMap.SimpleEntry<>(firstStr, firstInt));
        wordEntryList.add(new AbstractMap.SimpleEntry<>(secondStr, secondInt));

        // When
        wordPrinter.printWordsOrderByAppearanceDesc(wordEntryList);

        // Then
        InOrder inOrder = inOrder(console);
        inOrder.verify(console).printLine(format(WordPrinter.WORD_OUTPUT, secondStr, secondInt));
        inOrder.verify(console).printLine(format(WordPrinter.WORD_OUTPUT, firstStr, firstInt));


    }
}
