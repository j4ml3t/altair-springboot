package com.altair.exercise.domain;

import com.altair.exercise.infrastructure.ConsoleSlf4;
import com.altair.exercise.infrastructure.storage.FileStorage;
import com.altair.exercise.infrastructure.storage.TextFileReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

import static java.lang.String.format;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WordCounterTest {

    public static final String TEXT_TXT = "Text.txt";
    @Mock
    private ConsoleSlf4 console;
    private WordCounter wordCounter;
    private TextFileReader textFileReader;
    private WordPrinter wordPrinter;
    @Mock
    private TextFileName textFileName;
    @Mock
    private FileStorage fileStorage;

    @BeforeEach
    public void initialize() {
        textFileReader = new TextFileReader(fileStorage);
        wordPrinter = new WordPrinter(console);
        wordCounter = new WordCounter(textFileReader, wordPrinter, textFileName);
    }

    @Test
    public void Should_PrintWordOutputOrdered_When_ReadTextFile() throws IOException {

        // Given
        File fileToTest = ResourceUtils.getFile(format("src/test/resources/%s", TEXT_TXT));
        when(fileStorage.getFile(TEXT_TXT)).thenReturn(fileToTest);
        when(textFileName.getName()).thenReturn(TEXT_TXT);

        // When
        wordCounter.processWordsFromFile();

        // Then
        InOrder inOrder = inOrder(console);
        inOrder.verify(console).printLine("-  Word: lorem  >> Occurrences: 3");
        inOrder.verify(console).printLine("-  Word: ipsum  >> Occurrences: 2");
        inOrder.verify(console).printLine("-  Word: dolor  >> Occurrences: 1");
    }

}
